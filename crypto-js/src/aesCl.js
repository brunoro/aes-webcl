/**
 * Processes available data blocks using parallel EBC.
 *
 * This method invokes CL_doProcessBlock(offset), which must be implemented by a concrete subtype.
 *
 * @param {boolean} doFlush Whether all blocks and partial blocks should be processed.
 *
 * @return {WordArray} The processed data.
 *
 */

function initializeWebCL() {
   // First check if the WebCL extension is installed at all 
    if (window.WebCL == undefined) {
      throw "Unfortunately your system does not support WebCL. " +
            "Make sure that you have both the OpenCL driver " +
            "and the WebCL browser extension installed.";
    }

    // Setup WebCL context using the default device of the first platform 
    var platforms = WebCL.getPlatformIDs();
    var ctx = WebCL.createContextFromType ([WebCL.CL_CONTEXT_PLATFORM, 
                                           platforms[0]],
                                           WebCL.CL_DEVICE_TYPE_DEFAULT);

    return ctx;
}

// Compute SBOXes
var SBOX = [];
(function () {
    // Compute double table
    var d = [];
    for (var i = 0; i < 256; i++) {
        if (i < 128) {
            d[i] = i << 1;
        } else {
            d[i] = (i << 1) ^ 0x11b;
        }
    }

    // Walk GF(2^8)
    var x = 0;
    var xi = 0;
    for (var i = 0; i < 256; i++) {
        // Compute sbox
        var sx = xi ^ (xi << 1) ^ (xi << 2) ^ (xi << 3) ^ (xi << 4);
        sx = (sx >>> 8) ^ (sx & 0xff) ^ 0x63;
        SBOX[x] = sx;

        // Compute multiplication
        var x2 = d[x];
        var x4 = d[x2];
        var x8 = d[x4];

        // Compute next counter
        if (!x) {
            x = xi = 1;
        } else {
            x = x2 ^ d[d[d[x8 ^ x2]]];
            xi ^= d[d[xi]];
        }
    }
}());
  
var RCON = [0x00, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36];

function keySchedule(key) {
  var keyWords = key.words;
  var keySize = key.sigBytes / 4;

  // Compute number of rounds
  var nRounds = this._nRounds = keySize + 6

  // Compute number of key schedule rows
  var ksRows = (nRounds + 1) * 4;
  var ks = new Int8Array(ksRows);
  
  for (var ksRow = 0; ksRow < ksRows; ksRow++) {
      if (ksRow < keySize) {
          ks[ksRow] = keyWords[ksRow];
      } 
      else {
        var t = ks[ksRow - 1];

        if (!(ksRow % keySize)) {
            // Rot word
            t = (t << 8) | (t >>> 24);

            // Sub word
            t = (SBOX[t >>> 24] << 24) | (SBOX[(t >>> 16) & 0xff] << 16) | (SBOX[(t >>> 8) & 0xff] << 8) | SBOX[t & 0xff];

            // Mix Rcon
            t ^= RCON[(ksRow / keySize) | 0] << 24;
        } else if (keySize > 6 && ksRow % keySize == 4) {
            // Sub word
            t = (SBOX[t >>> 24] << 24) | (SBOX[(t >>> 16) & 0xff] << 16) | (SBOX[(t >>> 8) & 0xff] << 8) | SBOX[t & 0xff];
        }

        ks[ksRow] = ks[ksRow - keySize] ^ t;
      }
  }

  return { 'rounds': nRounds, 'keySchedule': ks, 'ksRows': ksRows }
}

CL_AES = {
  encrypt: function(msg, passwd) {
    var C = CryptoJS;

    var ctx = initializeWebCL();
    var params = C.kdf.OpenSSL.execute(passwd, 128/32, 128/32);
    var schedule = keySchedule(params['key']);
    
    // Reserve buffers
    var len = msg.length;

    var uint4     = 8 * 4;
    var state     = ctx.createBuffer(WebCL.CL_MEM_READ_WRITE, len * uint4);
    var roundKeys = ctx.createBuffer(WebCL.CL_MEM_READ_ONLY, schedule['ksRows'] * uint4);
    var rounds    = schedule['rounds'];

    // Create and build program for the first device
    var kernelSrc = loadKernel("clProgramAes");
    var program = ctx.createProgramWithSource(kernelSrc);
    var devices = ctx.getContextInfo(WebCL.CL_CONTEXT_DEVICES);

    program.buildProgram([devices[0]], "");

    // Create kernel and set arguments
    var kernel = program.createKernel("AES_encrypt");
    kernel.setKernelArg(0, state);
    kernel.setKernelArg(1, roundKeys);    
    kernel.setKernelArg(2, rounds, WebCL.types.UINT);

    // Create command queue using the first available device
    var cmdQueue = ctx.createCommandQueue (devices[0], 0);
    
    // Write the buffers to OpenCL device memory
    var typedMsg = new Int8Array(len);    
    for (var i = 0; i < len; i++)
      typedMsg[i] = msg[i];

    cmdQueue.enqueueWriteBuffer(state, false, 0, len, typedMsg, []);
    cmdQueue.enqueueWriteBuffer(roundKeys, false, 0, schedule['ksRows'], schedule['keySchedule'], []);
   
    // Init ND-range
    var local  = [128];
    var global = [len / 128];

    console.log("len: " + len + ", global: " + global + ", local: " + local);
    
    // Execute (enqueue) kernel
    cmdQueue.enqueueNDRangeKernel(kernel, global.length, [], global, local, []);

    // Read the result buffer from OpenCL device
    cmdQueue.enqueueReadBuffer(state, false, 0, len, typedMsg, []);    
    cmdQueue.finish(); //Finish all the operations

    return typedMsg;
  } 
}
