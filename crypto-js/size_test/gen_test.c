#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
    size_t times = atoi(argv[1]);
    size_t size  = 1048576;

    const void *data = malloc(size);

    char *filename = (char*) malloc(sizeof(char) * 1024);
    sprintf(filename, "file%zd", times);

    FILE* out = fopen(filename, "wb");

    size_t i;
    for (i = 0; i < times; i++)
        fwrite(data, size, 1, out);

    fclose(out);
    return 0;
}
