#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "aes.h"

#define BUFFER_LENGTH 1024

int main(int argc, char** argv) {
  AES_KEY key; 
  const unsigned char *passwd = "aes_passwd",
		      *input  = "gustavo";
  unsigned char* enc = malloc(sizeof(unsigned char) * BUFFER_LENGTH);
  unsigned char* dec = malloc(sizeof(unsigned char) * BUFFER_LENGTH);

  AES_set_encrypt_key((const unsigned char*) passwd, 256, &key); 
  AES_encrypt((const unsigned char*) input, enc, (const AES_KEY*) &key); 

  AES_set_decrypt_key((const unsigned char*) passwd, 256, &key); 
  AES_decrypt((const unsigned char*) enc, dec, (const AES_KEY*) &key); 

  printf("passwd: %s\n", passwd);
  printf("input:  %s\n", input);
  printf("enc:    %s\n", enc);
  printf("dec:    %s\n", dec);
  
  return 0;
}
